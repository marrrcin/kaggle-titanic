import pandas as pd
import re as regex
import numpy as np
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor, AdaBoostClassifier, ExtraTreesClassifier
from scipy.stats.mstats import winsorize
from sklearn.model_selection import train_test_split, cross_val_score, RandomizedSearchCV, GridSearchCV
from sklearn.naive_bayes import BernoulliNB, MultinomialNB, GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from xgboost import XGBClassifier
from sklearn.svm import SVC
import seaborn as sns

seed = 666


def log(str):
    print(str)


def cv(classifier, X_train, y_train):
    from time import time

    log("")
    log("===============================================")
    classifier_name = str(type(classifier).__name__)
    log("Testing " + classifier_name)
    now = time()

    log("Crossvalidating...")
    # recall = [cross_val_score(classifier, X_train, y_train, scoring="recall_micro", cv=10, n_jobs=-1)]
    accuracy = [cross_val_score(classifier, X_train, y_train, cv=10, n_jobs=-1)]
    # precision = [cross_val_score(classifier, X_train, y_train, scoring="precision_micro", cv=10, n_jobs=-1)]
    recall = -1
    precision = -1
    log("Crosvalidation completed in {0}s".format(time() - now))
    log("=================== Results ===================")
    log("Accuracy: " + str(accuracy))
    log("Precision: " + str(precision))
    log("Recall: " + str(recall))
    log("===============================================")
    log("CV time: {0}".format(time() - now))
    return accuracy[0], precision, recall


def predict_using_rf(training_data, test_data):
    classifier = RandomForestClassifier(random_state=seed, n_estimators=666, max_depth=5, criterion="entropy",
                                        n_jobs=-1)
    classifier.fit(training_data.iloc[:, 1:], training_data.iloc[:, 0])

    predictions = classifier.predict(test_data)
    return predictions


def predict_using_svc(training_data, test_data):
    classifier = SVC(random_state=seed, kernel="rbf", C=0.9)
    classifier.fit(training_data.iloc[:, 1:], training_data.iloc[:, 0])

    predictions = classifier.predict(test_data)
    return predictions


def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            log("Model with rank: {0}".format(i))
            log("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                results['mean_test_score'][candidate],
                results['std_test_score'][candidate]))
            log("Parameters: {0}".format(results['params'][candidate]))
            log("")


if __name__ == "__main__":
    df = pd.read_csv("train.csv", sep=";")
    test_df = pd.read_csv("test.csv", sep=";")
    test_ids = test_df.loc[:, "PassengerId"]

    # print(data.head())
    male_median_fares = {}
    female_median_fares = {}
    for pclass in [1, 2, 3]:
        male_median_fares[pclass] = df[(df.Sex == "male") & (df.Pclass == pclass)].Fare.median(skipna=True)
        female_median_fares[pclass] = df[(df.Sex == "female") & (df.Pclass == pclass)].Fare.median(skipna=True)

    columns_to_encode = ["Cabin", "Embarked", "Sex", "Title"]
    column_encoders = []

    columns_to_standarize = ["Age", "Fare"]
    column_standarizers = []

    age_regression_predictor = RandomForestRegressor(random_state=seed, n_estimators=666)
    age_regression_trained = {"trained": False, "columns": []}


    def prepare_data(data):
        data = data.drop(["Ticket", "PassengerId"], axis=1)
        for pclass in [1, 2, 3]:
            data.loc[
                (data.Pclass == pclass) & (data.Sex == "male") & ((data.Fare == 0) | (data.Fare.isnull())), "Fare"] = \
                male_median_fares[pclass]
            data.loc[
                (data.Pclass == pclass) & (data.Sex == "female") & ((data.Fare == 0) | (data.Fare.isnull())), "Fare"] = \
                female_median_fares[pclass]

        data["iskidtmp"] = df["Age"] <= 13
        data["IsKid"] = -1
        data.loc[data.iskidtmp == True, "IsKid"] = 1
        data.loc[data.iskidtmp == False, "IsKid"] = 0
        data = data.drop(["iskidtmp"], axis=1)

        data["isyoungtmp"] = df["Age"] <= 18
        data["IsYoung"] = -1
        data.loc[data.isyoungtmp == True, "IsYoung"] = 1
        data.loc[data.isyoungtmp == False, "IsYoung"] = 0
        data = data.drop(["isyoungtmp"], axis=1)

        data["alonetmp"] = df.loc[:, "SibSp"] + df.loc[:, "Parch"]
        data["TravelsAlone"] = 1
        data.loc[data.alonetmp > 0, "TravelsAlone"] = 0
        data = data.drop(["alonetmp"], axis=1)

        data.loc[:, "Cabin"].replace(regex=regex.compile(r"([A-Z])(.*)"), value=r"\1", inplace=True)
        data.Cabin.fillna("X", inplace=True)
        data.Embarked.fillna("X", inplace=True)
        data["FirstName"], data["SecondName"] = data["Name"].str.split(",").str
        data["Title"] = data["SecondName"].str.split(".").str[0].map(lambda title: title.strip())
        # print(data)
        data.loc[(data.Title != "Mr") & (data.Title != "Mrs") & (data.Title != "Miss") & (data.Title != "Master") & (
            data.Title != "Dr"), "Title"] = "Other"
        data = data.drop(["FirstName", "SecondName", "Name"], axis=1)
        for idx, col in enumerate(columns_to_encode):
            if len(column_encoders) != len(columns_to_encode):
                encoder = preprocessing.LabelEncoder()
                encoder.fit(data.loc[:, col])
                column_encoders.append(encoder)
            else:
                encoder = column_encoders[idx]

            data.loc[:, col] = encoder.transform(data.loc[:, col])

        if age_regression_trained["trained"] is False:
            age_prediction_columns = list(data.columns)
            age_prediction_columns.remove("Survived")
            age_prediction_columns.remove("Age")
            age_regression_trained["columns"] = age_prediction_columns

            which_rows = data.Age.isnull() == False
            age_regression_predictor.fit(data.loc[which_rows, age_prediction_columns], data.loc[which_rows, "Age"])
            age_regression_trained["trained"] = True
            log("Trained age regressor")

        log("Using age predictor")

        def predict_for_row(row):
            if np.isnan(row["Age"]) or row["Age"] < 1:
                return age_regression_predictor.predict(row[age_regression_trained["columns"]].reshape(1, -1))[0]
            else:
                return row["Age"]

        new_age_column = data.apply(predict_for_row, axis=1)
        data.loc[:, "Age"] = new_age_column.reshape(-1, 1)

        for idx, col in enumerate(columns_to_standarize):
            if len(column_standarizers) != len(columns_to_standarize):
                std = preprocessing.StandardScaler()
                std.fit(data.loc[:, col].reshape(-1, 1))
                column_standarizers.append(std)
            else:
                std = column_standarizers[idx]

            data.loc[:, col] = std.transform(data.loc[:, col].reshape(-1, 1))

        return data


    use_cache = 1

    if use_cache == 1:
        training_data = pd.read_csv("train_prepared.csv")
        test_df = pd.read_csv("test_prepared.csv")
    else:
        training_data = prepare_data(df)
        training_data.to_csv("train_prepared.csv", index=False)

        test_df = prepare_data(test_df)
        test_df.to_csv("test_prepared.csv", index=False)

    print(training_data.head())
    import random

    random.seed(seed)
    # X_train, X_test, y_train, y_test = train_test_split(training_data.iloc[:, 1:], training_data.iloc[:, 0],
    #                                                     train_size=0.7, stratify=training_data.iloc[:, 0],
    #                                                     random_state=seed)

    corr = training_data.loc[:, ["Age", "SibSp", "Parch", "Fare"]].corr()

    # sns.heatmap(corr.abs(),
    #             xticklabels=corr.columns.values,
    #             yticklabels=corr.columns.values,
    #             cmap=sns.diverging_palette(220, 10, as_cmap=True))
    #
    # sns.plt.show()

    # predictions = predict_using_svc(training_data, test_df)
    # output_df = pd.DataFrame({"PassengerId": test_ids, "Survived": predictions})
    # output_df.to_csv("results.csv", index=False)
    #
    # training_predicitons = training_data
    # training_predicitons["Predicted"] = predict_using_rf(training_data, training_data.iloc[:, 1:])
    # training_predicitons = training_predicitons[training_predicitons["Predicted"] != training_predicitons["Survived"]]
    # training_predicitons.to_csv("bad_predictions.csv", index=False)
    # exit(0)


    # Parameters for RF
    # log("RF:")
    # parameters = {
    #     "n_estimators":[103, 201, 305, 403, 666, 1001, 5007, 10001],
    #     "max_depth":[None, 5, 20, 40, 73, 100, 1000, 2000],
    #     "criterion":["gini", "entropy"]
    # }
    #
    # rand_search = RandomizedSearchCV(RandomForestClassifier(random_state=seed,n_jobs=1),param_distributions=parameters,
    #                                  n_iter=30,scoring="accuracy",
    #                                  n_jobs=-1,cv=10)
    # rand_search.fit(training_data.iloc[:,1:], training_data.iloc[:,0])
    # report(rand_search.cv_results_, 10)
    # exit(0)
    # log("SVC:")
    # parameters = {
    #     "C": [0.25, 0.9, 0.95, 1.0, 1.5, 2.0, 100.0, 1000.0],
    #     "kernel":["linear","rbf"],
    #     "tol":[0.1, 0.01, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]
    #
    # }
    # rand_search = RandomizedSearchCV(SVC(random_state=seed,cache_size=1024, max_iter=2000000),
    #                                  param_distributions=parameters,
    #                                  n_jobs=-1,
    #                                  n_iter=50,
    #                                  cv=10,
    #                                  scoring="accuracy")
    # rand_search.fit(training_data.iloc[:, 1:], training_data.iloc[:, 0])
    # report(rand_search.cv_results_, 10)

    # log("AdaBoost:")
    # parameters = {
    #     "n_estimators":[10, 20, 30, 50, 103, 200],
    #     "learning_rate":[0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    # }
    #
    # rand_search = RandomizedSearchCV(AdaBoostClassifier(random_state=seed),param_distributions=parameters,
    #                                  n_iter=36,scoring="accuracy",
    #                                  n_jobs=-1,cv=10)
    # rand_search.fit(training_data.iloc[:,1:], training_data.iloc[:,0])
    # report(rand_search.cv_results_, 10)


    # log("ExtraTrees:")
    # parameters = {
    #     "n_estimators":[10, 20, 30, 50, 103, 201, 305, 403, 666, 1001, 5007, 10001],
    #     "max_depth":[None, 5, 20, 40, 73, 100, 1000, 2000],
    #     "criterion":["gini", "entropy"],
    #     "min_samples_split": [2, 3, 4, 5, 6, 7, 8, 9],
    #     "bootstrap":[False, True]
    # }
    #
    # rand_search = RandomizedSearchCV(ExtraTreesClassifier(random_state=seed),param_distributions=parameters,
    #                                  n_iter=35,scoring="accuracy",
    #                                  n_jobs=-1,cv=10)
    # rand_search.fit(training_data.iloc[:,1:], training_data.iloc[:,0])
    # report(rand_search.cv_results_, 10)

    # log("XGB:")
    # parameters = {
    #     "n_estimators":[10, 20, 30, 50, 103, 201, 305, 666, 1001],
    #     "max_depth":[3, 5, 20, 40, 73, 103],
    #     "learning_rate":[0.1, 0.05, 0.01, 0.2],
    #     "subsample":[1.0, 0.5]
    # }
    #
    # rand_search = RandomizedSearchCV(XGBClassifier(seed=seed),param_distributions=parameters,
    #                                  n_iter=35,scoring="accuracy",
    #                                  n_jobs=2,cv=10)
    # rand_search.fit(training_data.iloc[:,1:], training_data.iloc[:,0])
    # report(rand_search.cv_results_, 10)
    # exit(0)

    # log("LogReg:")
    # parameters = {
    #     "C": [0.25, 0.9, 0.95, 1.0, 1.5, 2.0],
    #     "tol": [0.1, 0.01, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8],
    #     "dual": [True, False]
    #
    # }
    # rand_search = GridSearchCV(LogisticRegression(random_state=seed, max_iter=5000),
    #                            param_grid=parameters,
    #                            n_jobs=-1,
    #                            cv=10,
    #                            scoring="accuracy")
    # rand_search.fit(training_data.iloc[:, 1:], training_data.iloc[:, 0])
    # report(rand_search.cv_results_, 10)
    # exit(0)

    classifiers = [
        RandomForestClassifier(random_state=seed, n_estimators=103, n_jobs=-1, max_depth=5, criterion="gini"),
        SVC(random_state=seed, max_iter=-1, kernel="rbf", C=2.0, tol=0.001, probability=True),
        GaussianNB(),
        AdaBoostClassifier(random_state=seed, learning_rate=0.8, n_estimators=50),
        ExtraTreesClassifier(random_state=seed, criterion="entropy", n_estimators=10001, min_samples_split=6,
                             bootstrap=True, max_depth=2000, n_jobs=-1),
        LogisticRegression(random_state=seed, dual=False, tol=0.001, C=2.0, n_jobs=-1),
        XGBClassifier(seed=seed, n_estimators=1001, learning_rate=0.01, subsample=0.5, max_depth=20)
    ]

    stack_train_df = pd.DataFrame({"Survived": training_data.iloc[:, 0]})
    stack_test_df = pd.DataFrame()
    # stack_train_df = training_data
    # stack_test_df = test_df
    for idx, classifier in enumerate(classifiers):
        n = str(type(classifier).__name__) + "_" + str(idx)
        classifier.fit(training_data.iloc[:, 1:], training_data.iloc[:, 0])
        stack_train_df[n] = classifier.predict_proba(training_data.iloc[:, 1:])[:, 0]
        stack_test_df[n] = classifier.predict_proba(test_df)[:, 0]

    stack_train_df.head()
    stack_train_df.to_csv("stack.csv", index=False)

    # stack_train_df = pd.read_csv("stack.csv")
    # log("Stacked")
    # parameters = {
    #     "dual": [True, False],
    #     "C": [0.25, 0.5, 0.9, 1.0],
    #     "tol": [0.1, 0.01, 0.002, 1e-3, 1e-4, 1e-5, 1e-6]
    #
    # }
    #
    # grid_search = GridSearchCV(LogisticRegression(random_state=seed, max_iter=10000),
    #                            param_grid=parameters,
    #                            n_jobs=-1,
    #                            cv=10,
    #                            scoring="accuracy")
    # grid_search.fit(stack_train_df.iloc[:,1:], stack_train_df.iloc[:,0])
    # report(grid_search.cv_results_, 10)
    # exit(0)

    # log("Stacked RF")
    # parameters = {
    #     "n_estimators":[103, 201, 305, 403, 666, 1001, 5007, 10001],
    #     "max_depth":[None, 5, 20, 40, 73, 100, 1000, 2000],
    #     "criterion":["gini", "entropy"]
    # }
    #
    # rand_search = RandomizedSearchCV(RandomForestClassifier(random_state=seed,n_jobs=1),param_distributions=parameters,
    #                                  n_iter=15,scoring="accuracy",
    #                                  n_jobs=-1,cv=10)
    # rand_search.fit(training_data.iloc[:,1:], training_data.iloc[:,0])
    # report(rand_search.cv_results_, 10)
    # exit(0)

    stacked_classifier = LogisticRegression(random_state=seed, dual=True, tol=0.01, C=1.0)
    stacked_classifier.fit(stack_train_df.iloc[:, 1:], stack_train_df.iloc[:, 0])
    predictions = stacked_classifier.predict(stack_test_df)
    output_df = pd.DataFrame({"PassengerId": test_ids, "Survived": predictions})
    output_df.to_csv("results_stacked.csv", index=False)

    predictions_train = stacked_classifier.predict(stack_train_df.iloc[:, 1:])
    stack_train_df["Predicted"] = predictions_train

    training_predicitons = stack_train_df
    training_predicitons = training_predicitons[training_predicitons["Predicted"] != training_predicitons["Survived"]]
    training_predicitons.to_csv("bad_predictions_stacked.csv", index=False)
